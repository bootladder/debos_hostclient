CLEANUP = rm -f
MKDIR = mkdir -p

UNITY_ROOT=./link_to_unity_root
#HOST_C_COMPILER=arm-linux-gnueabihf-gcc
HOST_C_COMPILER=gcc
SIZE = arm-none-eabi-size

CFLAGS = -std=gnu99
CFLAGS += -Wall
CFLAGS += -Wextra
CFLAGS += -Werror
CFLAGS += -Wpointer-arith
CFLAGS += -Wcast-align
CFLAGS += -Wwrite-strings
CFLAGS += -Wswitch-default
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self
CFLAGS += -Wmissing-field-initializers
CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wundef
CFLAGS += -Wold-style-definition
CFLAGS += -Wmissing-prototypes
CFLAGS += -Wmissing-declarations
CFLAGS += -DUNITY_FIXTURES

TARGET_BOOTLOADER_PRODUCTION = debos-hostclient.out
TARGET_HOST_TEST = DeBos_HostClient_All_Tests.out
TARGET_HOST_PRODUCTION_LIB = libDeBos_HostClient_Production_Lib.a
LIB_ARGUMENT_TARGET_HOST_PRODUCTION_LIB = -L. -lDeBos_HostClient_Production_Lib

SRC_FILES_PRODUCTION=\
  src/Commands.c \
  src/WriteAppImage.c \
  src/ttySerial.c \
  src/Parse_Bootloader_Commandline.c \

SRC_FILES_PRODUCTION_BOOTLOADER=\
	src/DebOS_Bootloader.c \

SRC_FILES_TEST = \
  $(UNITY_ROOT)/src/unity.c \
  $(UNITY_ROOT)/extras/fixture/src/unity_fixture.c \
  test/all_tests.c \
  test/CommandsTest.c \
  test/Parse_Bootloader_CommandlineTest.c \
    \

SRC_FILES_TARGET_INTEGRATION_TEST = \
  $(UNITY_ROOT)/src/unity.c \
  $(UNITY_ROOT)/extras/fixture/src/unity_fixture.c \
  test/test_runners/Target_Integration_Tests.c \
  test/AppInterfaceCheckTest.c \
  test/WriteAppImageTest.c \
    \

SRC_FILES_MOCK = \
  test/mock/MockttySerial.c

INC_DIRS=-Isrc \
  -I$(UNITY_ROOT)/src -I$(UNITY_ROOT)/extras/fixture/src -I$(UNITY_ROOT)/extras/memory/src

INC_DIRS_MOCK=-Itest/mock

SYMBOLS=

all: test production clean

######################################################
production:
	@echo Building Bootloader executable
	$(HOST_C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(SRC_FILES_PRODUCTION)  \
				$(SRC_FILES_PRODUCTION_BOOTLOADER) -o $(TARGET_BOOTLOADER_PRODUCTION) 
	@mkdir -p bin; cp $(TARGET_BOOTLOADER_PRODUCTION) bin/
	@size bin/$(TARGET_BOOTLOADER_PRODUCTION)

######################################################
#create the object files for library
production_objects: $(SRC_FILES1) 
	@echo -e '\n'$@'\n' Compiling tested production objects for Host
	$(HOST_C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(SRC_FILES_PRODUCTION)  \
				$(SRC_FILES_PRODUCTION_BOOTLOADER) -c

$(TARGET_HOST_PRODUCTION_LIB): production_objects
	@echo -e '\n'$@'\n'  Building production library for testing
	@echo ar rcs  $(TARGET_HOST_PRODUCTION_LIB) *.o 
	@ar rcs  $(TARGET_HOST_PRODUCTION_LIB) *.o 

######################################################
#create test.out, run it on host
test: buildtest clean
	@echo -e '\n'$@'\n' Running Test Executable on Host
	./$(TARGET_HOST_TEST) -v

buildtest: $(SRC_FILES_TEST) $(TARGET_HOST_PRODUCTION_LIB)
	@echo  -e '\n'$@'\n' Building Test Executable for Host
	$(HOST_C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(INC_DIRS_MOCK) \
			$(SRC_FILES_TEST) $(SRC_FILES_MOCK) \
			\
			$(LIB_ARGUMENT_TARGET_HOST_PRODUCTION_LIB) \
				-o $(TARGET_HOST_TEST)

######################################################

clean:
	$(CLEANUP) *.o

