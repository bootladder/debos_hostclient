#include "Parse_Bootloader_Commandline.h"
#include "ttySerial.h"
#include <string.h>
#include <unistd.h>
#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP_RUNNER(Parse_Bootloader_Commandline)
{
    RUN_TEST_CASE(Parse_Bootloader_Commandline,ParseCommandline_InvalidCommand_Returns0);
    RUN_TEST_CASE(Parse_Bootloader_Commandline,ParseCommandline_ValidLoadCommand_SetsCommandVariable);
    RUN_TEST_CASE(Parse_Bootloader_Commandline,ParseCommandline_ValidEraseCommand_SetsCommandVariable);
    RUN_TEST_CASE(Parse_Bootloader_Commandline,ParseCommandline_ValidResetCommand_SetsCommandVariable);
}

TEST_GROUP(Parse_Bootloader_Commandline);
TEST_SETUP(Parse_Bootloader_Commandline){
    optind=1;
}
TEST_TEAR_DOWN(Parse_Bootloader_Commandline){}

TEST(Parse_Bootloader_Commandline,ParseCommandline_InvalidCommand_Returns0)
{
  const char * rawargs[10] = {"name","-z", "blah"};
  char * const * argv = (char * const *)rawargs;
  TEST_ASSERT_EQUAL_INT_MESSAGE(2, Parse_Bootloader_Commandline(3,argv), "invalid commandline");
}

TEST(Parse_Bootloader_Commandline,ParseCommandline_ValidLoadCommand_SetsCommandVariable)
{
  const char * rawargs[10] = {"name","-w", "test.bin"};
  char * const * argv = (char * const *)rawargs;
  TEST_ASSERT_EQUAL_INT_MESSAGE(1, Parse_Bootloader_Commandline(3,argv), "valid load command");

  TEST_ASSERT_EQUAL_INT_MESSAGE('w',Bootloader_Command,"bootloader command == w");
  TEST_ASSERT_EQUAL_INT_MESSAGE(0,strcmp((char*)Bootloader_AppImage,"test.bin"),"bootloader app image == argument");
}

TEST(Parse_Bootloader_Commandline,ParseCommandline_ValidEraseCommand_SetsCommandVariable)
{
  const char * rawargs[10] = {"name","-e"};
  char * const * argv = (char * const *)rawargs;
  TEST_ASSERT_EQUAL_INT_MESSAGE(1, Parse_Bootloader_Commandline(2,argv), "valid erasecommand");

  TEST_ASSERT_EQUAL_INT_MESSAGE('e',Bootloader_Command,"bootloader command == e");
}

TEST(Parse_Bootloader_Commandline,ParseCommandline_ValidResetCommand_SetsCommandVariable)
{
  const char * rawargs[10] = {"name","-R"};
  char * const * argv = (char * const *)rawargs;
  TEST_ASSERT_EQUAL_INT_MESSAGE(1, Parse_Bootloader_Commandline(2,argv), "valid reset command");

  TEST_ASSERT_EQUAL_INT_MESSAGE('R',Bootloader_Command,"bootloader command == R");
}
