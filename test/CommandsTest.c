#include "Commands.h"
#include <stdint.h>
#include "MockttySerial.h"
#include <unistd.h>
#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP_RUNNER(Commands) {
    RUN_TEST_CASE(Commands, WaitForBytesReceived_2bytesreceived_FillsBufferReturns1);
    RUN_TEST_CASE(Commands, WaitForBytesReceived_timeout_Returns2);
}

static uint8_t recvbuf[256];
static uint8_t buf[256];

TEST_GROUP(Commands);

TEST_SETUP(Commands) {
    ttySerialOpen((char *) 0);
    for (int i = 0; i < 256; i++)
        buf[i] = i;
}

TEST_TEAR_DOWN(Commands) {
    usleep(100000);
}

TEST(Commands, WaitForBytesReceived_2bytesreceived_FillsBufferReturns1) {
    mockRxBuf[0] = 3;
    mockRxBuf[1] = 3;
    mockRxPtrEnd = 2;
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, Commands_WaitForBytesReceived(recvbuf, 2, 100), "did not return 1");
    TEST_ASSERT_EQUAL_INT_MESSAGE(3, recvbuf[0], "first byte not read");
    TEST_ASSERT_EQUAL_INT_MESSAGE(3, recvbuf[1], "second byte not read");
}

TEST(Commands, WaitForBytesReceived_timeout_Returns2) {
    mockRxBuf[0] = 3;
    mockRxPtrEnd = 1;
    TEST_ASSERT_EQUAL_INT_MESSAGE(2, Commands_WaitForBytesReceived(recvbuf, 2, 1000), "did not return 2");
}
