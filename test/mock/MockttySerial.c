#include "MockttySerial.h"

static int tty_read_fd;
static int tty_write_fd;

char ttyFilename[100] = "/dev/ttyUSB0";

uint8_t mockRxBuf[512];
uint16_t mockRxPtr = 0;
uint16_t mockRxPtrEnd = 0;

int ttySerialWrite(uint8_t *buf, uint16_t size) {
    (void) buf;
    (void) size;
    return 1;
}

int ttySerialOpen(char *filename) {
    (void) filename;
    tty_write_fd = 7;
    tty_read_fd = 7;
    mockRxPtr = 0;
    return 1;
}

int ttySerialClose(void) {
    return 1;
}

int ttySerialRead(uint8_t *buf, uint16_t size) {
    if (mockRxPtr == mockRxPtrEnd)
        return 0;
    for (uint16_t i = 0; i < size; i++) {
        buf[i] = mockRxBuf[mockRxPtr++];
        if (mockRxPtr >= 512)
            mockRxPtr = 0;
    }
    return 1;
}

