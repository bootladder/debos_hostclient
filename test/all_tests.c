#include "unity_fixture.h"

static void RunAllTests(void)
{
  RUN_TEST_GROUP(Commands);
  RUN_TEST_GROUP(Parse_Bootloader_Commandline);
}

int main(int argc, const char * argv[])
{
  return UnityMain(argc, argv, RunAllTests);
}
