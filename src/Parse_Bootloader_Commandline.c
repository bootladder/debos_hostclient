#include "Parse_Bootloader_Commandline.h"
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>

uint8_t Bootloader_Command;
uint8_t Bootloader_AppImage[100];
char Bootloader_SerialDevicePath[100];
uint8_t Bootloader_SignalArgument[100];
uint8_t Bootloader_SignalNumber;

static const char *short_options = "hew:RHd:LS:A:";

static void print_help(void) {
    printf(" (e)rase   (w)rite   (d)evice (R)eset (H)alt (L)ist Signals (S)end Signal Number   Signal (A)rgument \n");
    return;
}

int Parse_Bootloader_Commandline(int argc, char *const argv[]) {
    int c;
    Bootloader_Command = 0;

    if (argc == 1) return 0;

    while ((c = getopt(argc, argv, short_options)) != -1) {
        switch (c) {
            case 'h':
                print_help();
                break;

            case 'w':
                //memcpy(Bootloader_FunitPath,optarg,strlen(optarg));
                if (strlen(optarg) > 256) return 2;
                memcpy(Bootloader_AppImage, optarg, strlen(optarg));
                Bootloader_Command = 'w';

                //valid write command.
                //if( argc < 3 )  return 2;
                //if( argv[2][0] == 0) return 2;
                //if( strlen(argv[2]) > 256) return 2;

                //valid filename.  go ahead and do the load command
                //Bootloader_Command = 'w';
                //memcpy(Bootloader_AppImage,argv[2],strlen(argv[2]));
                break;

            case 'e':
                Bootloader_Command = 'e';
                break;

            case 'R':
                Bootloader_Command = 'R';
                break;

            case 'H':
                Bootloader_Command = 'H';
                break;

            case 'd':
                memcpy(Bootloader_SerialDevicePath, optarg, strlen(optarg));
                break;

            case 'L':
                Bootloader_Command = 'L';
                break;

            case 'S': {
                // The signal number is ASCII.  Convert it to binary
                Bootloader_SignalNumber = atoi(optarg);
                Bootloader_Command = 'S';
                printf("Sending a signal to the target\n");
                printf("Signal Number : %d\n", Bootloader_SignalNumber);
                break;
            }

            case 'A':
                // does not affect the command, it will still be SendSignal
                memcpy(Bootloader_SignalArgument, optarg, strlen(optarg));
                printf("Signal Argument: %d\n", *Bootloader_SignalArgument);
                break;
            default:
                break;
        }
    }
    if (Bootloader_Command == 0) {
        return 2;
    }
    (void) argc;
    (void) argv;
    return 1;
}
