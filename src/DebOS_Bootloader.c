#include "Parse_Bootloader_Commandline.h"
#include "WriteAppImage.h"
#include "Commands.h"
#include "ttySerial.h"
#include <stdio.h>

int main(int argc, char *const argv[]) {
    printf("\nDebOS Bootloader v0.1...\n");
    if (1 != Parse_Bootloader_Commandline(argc, argv)) {
        printf("invalid commandline\n");
        return 0;
    }

    if (0 == ttySerialOpen(Bootloader_SerialDevicePath)) {
        printf("Could not open ttyUSB\n");
        return 0;
    }

    switch (Bootloader_Command) {
        case 'w': {
            int ret = WriteAppImage((char *) Bootloader_AppImage);
            if (ret == 0) {
                printf("fail\n");
                return 0;
            }
            if (ret == 2) {
                printf("Bad Filename\n");
                return 0;
            } else if (ret == 3) {
                printf("Image too short\n");
                return 0;
            } else if (ret == 4) {
                printf("Timeout waiting for response\n");
                return 0;
            } else if (ret == 5) {
                printf("write page failed\n");
                return 0;
            }

            printf("successfully wrote App Image to Target!\n");
            break;
        }

        case 'H': {
            Commands_SendHalt();
            break;
        }
        case 'R': {
            Commands_SendReset();
            break;
        }
        case 'L': {
            Commands_SendListSignals();
            break;
        }
        case 'S': {
            Commands_SendSignal(Bootloader_SignalNumber, Bootloader_SignalArgument);
            break;
        }
        default:
            return 0;
    }

}

