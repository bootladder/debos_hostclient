#include <stdint.h>

int Commands_SendHalt(void);
int Commands_SendReset(void);
int Commands_SendWriteFlash256(uint32_t addr, uint8_t * buf);
int Commands_SendReadFlash256(uint32_t addr, uint8_t * buf);

int Commands_WaitForBytesReceived(uint8_t * buf, uint8_t num, uint32_t timeout);
int Commands_SendListSignals(void);
int Commands_SendSignal(int sigNum, uint8_t * sigArg);

