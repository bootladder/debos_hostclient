#include <stdint.h>


int Parse_Bootloader_Commandline(int argc, char * const argv[]);

extern uint8_t Bootloader_Command;
extern uint8_t Bootloader_AppImage[100];
extern uint8_t Bootloader_SignalArgument[100];
extern uint8_t Bootloader_SignalNumber;

extern char Bootloader_SerialDevicePath[100];

