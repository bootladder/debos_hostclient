#include <stdint.h>

int WriteAppImage(char * filename);
int SeekAppImageToAddress(uint32_t address);
int OpenAppImage(char * filename);
int CloseAppImage(void);
int GetFileLength(void);
