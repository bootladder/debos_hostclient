#include "ttySerial.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

static void config_termios(int fd);

static int tty_read_fd;
static int tty_write_fd;

char ttyFilename[100] = "/dev/ttyUSB0";

int ttySerialWrite(uint8_t *buf, uint16_t size) {
    int retval = write(tty_write_fd, buf, size);
    if (retval < 0) {
        fprintf(stderr, "cant write to ttyUSB"); //bad!
        close(tty_write_fd);
        return 0;
    }
    return 1;
}

int ttySerialOpen(char *filename) {
    if (*filename == 0) {
        fprintf(stderr, "must supply path to device");
    }
    tty_write_fd = open(filename, O_WRONLY | O_NOCTTY);
    tty_read_fd = open(filename, O_RDONLY | O_NOCTTY | O_NONBLOCK);

    config_termios(tty_write_fd);

    if (tty_write_fd > 0 && tty_read_fd > 0) {
        fprintf(stderr, "opened ttyUSB for (NONBLOCKING)read and writing: %s ; fd=%d\n",
                filename, tty_write_fd);
        fflush(stdout);
        return 1;
    } else {
        fprintf(stderr, "failed opening serial device\n");
        return 0;
    }

}

int ttySerialClose(void) {
    close(tty_write_fd);
    close(tty_read_fd);
    return 1;
}

int ttySerialRead(uint8_t *buf, uint16_t size) {
    int retval = read(tty_read_fd, buf, size);
    if (retval < 0) {
        return 0;
    }
    return 1;
}

static void config_termios(int fd) {
    struct termios config;
    // Check if the file descriptor is pointing to a TTY device or not.
    if (!isatty(fd)) {}
    // Get the current configuration of the serial interface
    if (tcgetattr(fd, &config) < 0) {}

    // Input flags - Turn off input processing
    //
    // convert break to null byte, no CR to NL translation,
    // no NL to CR translation, don't mark parity errors or breaks
    // no input parity check, don't strip high bit off,
    // no XON/XOFF software flow control
    //
    config.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
                        INLCR | PARMRK | INPCK | ISTRIP | IXON);

    //
    // Output flags - Turn off output processing
    //
    // no CR to NL translation, no NL to CR-NL translation,
    // no NL to CR translation, no column 0 CR suppression,
    // no Ctrl-D suppression, no fill characters, no case mapping,
    // no local output processing
    //
    // config.c_oflag &= ~(OCRNL | ONLCR | ONLRET |
    //                     ONOCR | ONOEOT| OFILL | OLCUC | OPOST);
    config.c_oflag = 0;

    //
    // No line processing
    //
    // echo off, echo newline off, canonical mode off,
    // extended input processing off, signal chars off
    //
    config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

    //
    // Turn off character processing
    //
    // clear current char size mask, no parity checking,
    // no output processing, force 8 bit input
    //
    config.c_cflag &= ~(CSIZE | PARENB);
    config.c_cflag |= CS8;

    //
    // One input byte is enough to return from read()
    // Inter-character timer off
    //
    config.c_cc[VMIN] = 1;
    config.c_cc[VTIME] = 0;

    //
    // Communication speed (simple version, using the predefined
    // constants)
    //
    //if(cfsetispeed(&config, B9600) < 0 || cfsetospeed(&config, B9600) < 0) {
    if (cfsetispeed(&config, B115200) < 0 || cfsetospeed(&config, B115200) < 0) {

    }

    //
    // Finally, apply the configuration
    //
    if (tcsetattr(fd, TCSAFLUSH, &config) < 0) {}


}


