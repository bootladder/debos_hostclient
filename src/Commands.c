#include "Commands.h"
#include "ttySerial.h"
#include "Protocol.h"
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>

int Commands_SendHalt(void) {
    //type is 1
    uint8_t message[1];
    message[0] = 1;

    if (1 == ttySerialWrite(message, 1))
        return 1;

    return 0;
}

int Commands_SendReset(void) {
    //type is 2
    uint8_t message[1];
    message[0] = 2;

    if (1 == ttySerialWrite(message, 1))
        return 1;

    return 0;
}

int Commands_SendWriteFlash256(uint32_t addr, uint8_t *buf) {
    //type is 3
    uint8_t message[261];
    message[0] = 3;
    //address in network order
    uint32_t networkaddress = htonl(addr);
    for (int i = 0; i < 4; i++) {
        message[1 + i] = networkaddress & 0xFF;
        networkaddress >>= 8;
    }
    memcpy(&(message[5]), buf, 256);

    if (1 == ttySerialWrite(message, 261))
        return 1;


    (void) addr;
    (void) buf;
    (void) lengths_of_types;
    return 0;
}

int Commands_SendReadFlash256(uint32_t addr, uint8_t *buf) {
    //type is 4
    uint8_t message[5];
    message[0] = 4;
    //address in network order
    uint32_t networkaddress = htonl(addr);
    for (int i = 0; i < 4; i++) {
        message[1 + i] = networkaddress & 0xFF;
        networkaddress >>= 8;
    }
    if (1 == ttySerialWrite(message, 5))
        return 1;

    (void) addr;
    (void) buf;
    return 0;
}
//////////////////////////////////////////////////////

int Commands_WaitForBytesReceived(uint8_t *buf, uint8_t num, uint32_t timeout) {
    //start timer
    uint8_t numreceived = 0;
    uint16_t sleepcount = 0;
    while (numreceived < num) {

        if (1 == ttySerialRead(buf, 1)) {
            numreceived++;
            buf++;
        } else {
            usleep(1000);
            sleepcount++;
            if (sleepcount == timeout)
                return 2;
        }

    }
    (void) buf;
    (void) num;
    (void) timeout;
    return 1;
}

/////////////////////////////////////////////////////////

int Commands_SendListSignals(void) {
    //type is 8
    uint8_t message[1];
    message[0] = 8;

    if (1 == ttySerialWrite(message, 1))
        return 1;

    return 0;
}

// for now just 1 argument byte
int Commands_SendSignal(int sigNum, uint8_t *sigArg) {
    // this is the serial message
    uint8_t message[3];
    message[0] = 9; //type is 9
    message[1] = sigNum;  //signal number
    message[2] = *sigArg; //signal data, 1 byte
    //memcpy(&message[2], sigArg, strlen((const char *)sigArg));  // TBD

    if (1 == ttySerialWrite(message, 3))
        return 1;

    return 0;
}