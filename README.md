# 2017-10-23
# Host Client for DebOS #

`DebOS_Bootloader -w app.bin`

# app.bin must be linked with the supplied Linker Script

Your actual App main function is linked to 0x2100
Your vector table is linked to 0x2000

Your App function must never block for more than the specified time, or the system watchdog will trip.

If you want your RAM loaded from Flash, you have to do that yourself in your app.
The system will clear your RAM at reset and do nothing more for your app startup
App RAM starts at 0x20001000, below that is system RAM.

The System Checkpoint Pointer is located at 0x1FF0, see system header file

Assumes you've got a device called /dev/ttyUSB0 and it's already to set to 115200 baud.
