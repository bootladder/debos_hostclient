#include "WriteAppImage.h"
#include <stdint.h>
#include "MockttySerial.h"
#include <unistd.h>

#include "unity.h"
#include "unity_fixture.h"
#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP_RUNNER(WriteAppImage)
{
    RUN_TEST_CASE(WriteAppImage,OpenAppImage_GoodFile_ReturnsFDnonzero);
    RUN_TEST_CASE(WriteAppImage,OpenAppImage_BadFile_Returns0);
    RUN_TEST_CASE(WriteAppImage,SeekAppImageToAddress_ValidAddress_ReturnsOffset);
    RUN_TEST_CASE(WriteAppImage,SeekAppImageToAddress_InvalidAddress_Returns0);
    RUN_TEST_CASE(WriteAppImage,GetFileLength_GoodFile_ReturnsLength);
    RUN_TEST_CASE(WriteAppImage,WriteAppImage_BadFile_Returns2);
    RUN_TEST_CASE(WriteAppImage,WriteAppImage_AppTooShort_Returns3);
    RUN_TEST_CASE(WriteAppImage,WriteAppImage_GoodApp_Returns1);

}


static uint8_t recvbuf[256];
static uint8_t buf[256];

char goodfilename[10] = "test.bin";
char tooshortimagefilename[30] = "truncatedtest.bin";
char badfilename[20] = "notexist.bin";

TEST_GROUP(WriteAppImage);
TEST_SETUP(WriteAppImage)
{
  CloseAppImage();
  ttySerialOpen((char *) 0); //doesnt matter
  for(int i=0;i<256;i++)
    buf[i] = i;
  (void)recvbuf;
}
TEST_TEAR_DOWN(WriteAppImage){
}

TEST(WriteAppImage,OpenAppImage_GoodFile_ReturnsFDnonzero)
{
  int blah = OpenAppImage(goodfilename);
  TEST_ASSERT_MESSAGE(blah>0,"File exists");
}
TEST(WriteAppImage,OpenAppImage_BadFile_Returns0)
{
  TEST_ASSERT_EQUAL_INT_MESSAGE(0,OpenAppImage(badfilename),"File Not exists");
}
TEST(WriteAppImage,SeekAppImageToAddress_ValidAddress_ReturnsOffset)
{
  int blah = OpenAppImage(goodfilename);
  (void)blah;
  TEST_ASSERT_EQUAL_INT_MESSAGE(0x1000,SeekAppImageToAddress(0x1000),"seek");
}
IGNORE_TEST(WriteAppImage,SeekAppImageToAddress_InvalidAddress_Returns0)
{
}
TEST(WriteAppImage,GetFileLength_GoodFile_ReturnsLength)
{
  int blah = OpenAppImage(tooshortimagefilename);
  (void)blah;
  TEST_ASSERT_EQUAL_INT_MESSAGE(1024,GetFileLength(),"get file length");
}
TEST(WriteAppImage,WriteAppImage_BadFile_Returns2)
{
  TEST_ASSERT_EQUAL_INT_MESSAGE(2,WriteAppImage(badfilename),"writeappimage badfile");
}
TEST(WriteAppImage,WriteAppImage_AppTooShort_Returns3)
{
  TEST_ASSERT_EQUAL_INT_MESSAGE(3,WriteAppImage(tooshortimagefilename),"writeappimage tooshortimage file");
}
IGNORE_TEST(WriteAppImage,WriteAppImage_GoodApp_Returns1)
{
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,WriteAppImage(goodfilename),"writeappimage good app");
	uint8_t buf[1];
	ttySerialRead(buf,1);
	ttySerialRead(buf,1);
	ttySerialRead(buf,1);
	ttySerialRead(buf,1);
}
