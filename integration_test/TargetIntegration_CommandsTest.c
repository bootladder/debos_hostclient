#include "Commands.h"
#include <stdint.h>
#include "ttySerial.h"
#include <unistd.h>

#include "unity.h"
#include "unity_fixture.h"

  static uint8_t recvbuf[256];
  static uint8_t buf[256];

TEST_GROUP(Commands);
TEST_SETUP(Commands)
{
  ttySerialOpen();
  for(int i=0;i<256;i++)
    buf[i] = i;
}
TEST_TEAR_DOWN(Commands){

	usleep(100000);
}

TEST(Commands,Reset_ReceivesPowerOnMessage)
{
  TEST_ASSERT_EQUAL(1,Commands_SendReset());
	usleep(500000);
  ttySerialRead(recvbuf,1);
  ttySerialRead(recvbuf,1);
  ttySerialRead(recvbuf,1);
  ttySerialRead(recvbuf,1);
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL(0x55,recvbuf[0]);
}

TEST(Commands,Halt_ReceivesResponse)
{
  TEST_ASSERT_EQUAL(1,Commands_SendHalt());

  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,recvbuf[0] , "Halt response type()");
}

TEST(Commands,RepeatedHalt_ReceivesRepeatedResponse)
{
  TEST_ASSERT_EQUAL(1,Commands_SendHalt());

  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,recvbuf[0] , "Halt response type()");

  TEST_ASSERT_EQUAL(1,Commands_SendHalt());
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,recvbuf[0] , "Halt response type()");
}

TEST(Commands,WriteFlash_InvalidAddr_ReceivesResponse)
{
  //Test address 0x11111111
  TEST_ASSERT_EQUAL(1,Commands_SendWriteFlash256(0x00110011,buf));

  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(3,recvbuf[0] , "Write response type()");
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(2,recvbuf[0] , "Write response status");

  //Test address 0x1111
  TEST_ASSERT_EQUAL(1,Commands_SendWriteFlash256(0x1111,buf));

  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(3,recvbuf[0] , "Write response type()");
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(2,recvbuf[0] , "Write response status");

}

TEST(Commands,WriteFlash_ValidAddr_ReceivesResponse)
{
  TEST_ASSERT_EQUAL(1,Commands_SendWriteFlash256(0x12F00,buf));

  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(3,recvbuf[0] , "Write response type()");
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,recvbuf[0] , "Write response status");
}
TEST(Commands,ReadFlash_InvalidAddr_ReceivesResponse)
{
  //Read Flash
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,Commands_SendReadFlash256(0x1111,recvbuf) , "ReadFlash256()");
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(4,recvbuf[0] , "read response type");
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(2,recvbuf[0] , "read status");
}

TEST(Commands,ReadFlash_ValidAddr_ReceivesResponse)
{
  //Read Flash
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,Commands_SendReadFlash256(0x3000,recvbuf) , "ReadFlash256()");
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(4,recvbuf[0] , "read response type");
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,recvbuf[0] , "read status");

  //read 256 bytes
  for(int i=0;i<256;i++)
    ttySerialRead(recvbuf,1);
}

TEST(Commands,WriteFlashReadFlash_Works)
{
  TEST_ASSERT_EQUAL(1,Commands_SendWriteFlash256(0x2100,buf));

  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(3,recvbuf[0] , "Write response type()");
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,recvbuf[0] , "Write response status");
  
  //Read Flash
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,Commands_SendReadFlash256(0x2100,recvbuf) , "ReadFlash256()");

  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(4,recvbuf[0] , "read response type");
  ttySerialRead(recvbuf,1);
  TEST_ASSERT_EQUAL_INT_MESSAGE(1,recvbuf[0] , "read status");

  printf("starting to read bufs\n");
  for(int i=0;i<256;i++)
  {
    ttySerialRead(recvbuf,1);
    TEST_ASSERT_EQUAL_INT_MESSAGE(recvbuf[0],buf[i] , "read write buffers");
  }
}
