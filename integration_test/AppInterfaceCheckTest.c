
#include "Commands.h"
#include <stdint.h>
#include "ttySerial.h"
#include <unistd.h>
#include <arpa/inet.h>

#include "unity.h"
#include "unity_fixture.h"

#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP_RUNNER(AppInterfaceCheck) {
    RUN_TEST_CASE(AppInterfaceCheck, CheckForCheckpointPointer);
}

static uint8_t recvbuf[256];
static uint8_t buf[256];

TEST_GROUP(AppInterfaceCheck);

TEST_SETUP(AppInterfaceCheck) {
    ttySerialOpen((char *) 0);
    for (int i = 0; i < 256; i++)
        buf[i] = i;
}

TEST_TEAR_DOWN(AppInterfaceCheck) {

}

TEST(AppInterfaceCheck, CheckForCheckpointPointer) {
    //Read Flash
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, Commands_SendReadFlash256(0x1F00, recvbuf), "ReadFlash256()");
    ttySerialRead(recvbuf, 1);
    TEST_ASSERT_EQUAL_INT_MESSAGE(4, recvbuf[0], "read response type");
    ttySerialRead(recvbuf, 1);
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, recvbuf[0], "read status");

    //read 256 bytes
    uint8_t temp[1];
    for (int i = 0; i < 256; i++) {
        ttySerialRead(temp, 1);
        printf("%02X", temp[0]);
        recvbuf[i] = temp[0];
    }

    uint32_t *addrptr = (uint32_t *) &(recvbuf[0xF0]);
    uint32_t addr = *addrptr;
    //printf("%02X %02X %02X %02X",recvbuf[0xF0],recvbuf[0xf1],recvbuf[0xf2],recvbuf[0xf3]);
    //for(int i=0;i<256;i++)printf("%02X",recvbuf[i]);

    TEST_ASSERT_MESSAGE(addr < 0x02000, "valid pointer, less than 0x2000");
    TEST_ASSERT_MESSAGE(addr > 0, "valid pointer, nonzero");


}
